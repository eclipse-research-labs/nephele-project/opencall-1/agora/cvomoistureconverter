# -*- coding: utf-8 -*-

import csv
from datetime import datetime
from influxdb_client import InfluxDBClient, Point
from influxdb_client.client.write_api import SYNCHRONOUS
import sys

# InfluxDB credentials and connection details
org = "wot"  # Replace with your organization name
token = "my-token"  # Replace with your InfluxDB token
url = "http://localhost:8086"  # Replace with your InfluxDB URL

properties_dict={
    'neutrons': 'neutrons',
    'muons': 'muons',
    'gamma': 'gamma',
    'integration_time(s)': 'integration_time',
    'V_in(Volt)': 'v_in',
    'temperature_in(°C)': 'temperature_in',
    'temperature_ext(°C)': 'temperature_ext',
    'ur(%)': 'ur',
    'pressure(hPa)': 'pressure'
}


# CSV file path
csv_file = sys.argv[1] #'data.csv'  # Replace with your CSV file path

print("Loading csv: "+csv_file+" to "+url)

# Initialize InfluxDB client
client = InfluxDBClient(url=url, token=token)
write_api = client.write_api(write_options=SYNCHRONOUS)

num_records=0
with open(csv_file, 'r', encoding='utf-8') as file:
    reader = csv.DictReader(file, delimiter=';')
    num_records = sum(1 for row in reader)

# Re-read CSV and write data to respective buckets
with open(csv_file, 'r', encoding='utf-8') as file:
    reader = csv.DictReader(file, delimiter=';')
    total=0
    counter=0
    delta=round(num_records/10)
    last_timestamp = None
    print("Uploading "+str(num_records)+" records...")
    for row in reader:
        timestamp = datetime.strptime(row['#Datetime'], '%Y-%m-%d %H:%M:%S')
        
        if last_timestamp and timestamp == last_timestamp:
            print(f"Skipping record with duplicate timestamp: {timestamp}")
            continue

        print("debug: uploading record: "+str(total))

        # Write each column's data to its corresponding bucket
        for header, value in row.items():
            if header == '#Datetime':
                continue
            mapped_header=properties_dict[header]
            bucket_name = f"{mapped_header.strip()}"
            # Write point to InfluxDB
            try:
                point = Point(mapped_header.strip()) \
                    .time(timestamp, 's')
                if header=="neutrons" or header=="muons" or header=="gamma" or header=="integration_time(s)":
                    safeValue= value.rsplit('.', 1)[0]
                    point.field("value", int(safeValue))  # Assuming all values are numeric, adjust as per data type
                else:
                    point.field("value", float(value)) 
                write_api.write(bucket_name, org, point)
            except Exception as e:
                print(f"Failed to write {mapped_header} for datetime: {timestamp}")
                print(e)
        
        last_timestamp=timestamp

        #Print counter for user
        if counter>delta:
            counter=0
            print("Uploaded "+str(total)+"/"+str(num_records))
        else:
            counter=counter+1
        total=total+1

# Close connection
client.close()
print("Uploaded "+str(num_records)+" records to influxDB!")