#!/bin/sh
echo "Setting vo inlfaxDB yaml credential"
echo "" >> /app/cvo_mc_draft.yaml
echo "    address: ${influx_address}" >> /app/cvo_mc_draft.yaml
echo "    dbUser: ${influx_dbUser}" >> /app/cvo_mc_draft.yaml
echo "    dbPass: ${influx_dbPass}" >> /app/cvo_mc_draft.yaml
echo "    dbToken: ${influx_dbToken}" >> /app/cvo_mc_draft.yaml
cat /app/cvo_mc_draft.yaml
echo "Start Vo"
vo-wot -t mc_td.json -f cvo_mc_draft.yaml mc.py