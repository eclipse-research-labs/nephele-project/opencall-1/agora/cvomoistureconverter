# cVoCRNS
This project contains the code and implementation for the representation of the CRNS cVO soil moisture conversion model within the Web of Things (WoT) framework using wotpy. The goal of this module is to **convert historical neutron count data** provided by the CRNS VO **to soil moisture data** using an R model wrapped inside the cVO.  
This Composite Virtual Object (cVo) can be used directly on your localhost or with Docker. We recommend using the Docker version, as running the code directly on your operating system, on Windows-based systems, can encounter difficulties.

**Important note**: this module requires a running VO CRNS instance in order to be used. Follow this [guide](https://gitlab.eclipse.org/eclipse-research-labs/nephele-project/opencall-1/agora/vocrns/-/blob/beta/README.md?ref_type=heads) to setup and run a VO CRNS instance before proceeding with installation.

## Installation
### Prerequisites:
- Python
- R
- Docker
### Steps
1. Run the script init.sh to download and install required dependencies:
```
./scripts/init.sh
```
2. Set python path to point to the root folder of this repo:
```
ENV PYTHONPATH=/my-path-to-repo/cvomoistureconverter
```
3. Create a .env file in the root folder of this repo and set the following variables (Note: SCRIPT_PATH and FILE_IO path must be absolute):
```
host=localhost
catalogue_port=9091
http_port=8081
vo_address=http://localhost:9090/crns-sensor

influx_address=http://localhost:8086
influx_dbUser=my-username
influx_dbPass=my-password
influx_dbToken=my-token

SCRIPT_PATH=/my-path-to-repo/neutrons2soilmoisture/model/processing_imola_v0001.R
FILEIO_PATH=/my-path-to-repo/neutrons2soilmoisture/dataIO
```


## Usage
1. Start the cVO running the script: 
```
./scripts/runCVoMC.sh
```
2. In order for the R model to function properly, the influx db instance must be initialized with **1 year of past historical data**:
- Run the 'batch_load_crns_history' script without any parameter to upload 1 year of test data to localhost:8086:
```
python batch_load_crns_history
```
- Alternatively, specify the csv path to upload custom csv data:
```
python batch_load_crns_history -h http://localhost:8086 ./custom.data.csv
```
3. The cVO is now ready to use. To test if it is working correctly, start the python client tester by running the script runTestClient.sh:
```
./scripts/runTestClient.sh
```
Or directly call the "convert" action via http to fetch the model output


## Docker
This module can be built running the script: ./scripts/dockerbuild.sh.  
A docker compose file can be used to deploy the whole stack at once. Env variables can be specified directly in the docker-compose.yml file or using the same env file presented in the paragraph "Installation/Steps". Here is an example docker-compose file:
```
version: "3.3"

volumes:
  influxdb-data:
  influxdb-config:

services:
  cvo:
    image: vaimeedock/cvo-crns:latest
    networks:
      - default
    environment:
      - host=localhost
      - catalogue_port=9091
      - http_port=8081
      - externalCert=false
      - vo_address=http://vocrns:9090/crns-sensor
      - SCRIPT_PATH=/app/neutrons2soilmoisture/model/processing_imola_v0001.R
      - FILEIO_PATH=/app/neutrons2soilmoisture/dataIO

      - influx_address=http://influx:8086
      - influx_dbUser=my-username
      - influx_dbPass=my-password
      - influx_dbToken=my-token
    ports:
      - 9091:9091
      - 8081:8081
    depends_on:
      - vocrns
    restart: always



  vocrns:
    image: vaimeedock/vocrns:latest
    networks:
      - default
    environment:
      - host=localhost
      - catalogue_port=9090
      - http_port=8080
      - externalCert=false
      - MQTT_TOPIC=testing/crns/67/1
      - MQTT_HOST=mqtt://broker.emqx.io

      - influx_address=http://influx:8086
      - influx_dbUser=my-username
      - influx_dbPass=my-password
      - influx_dbToken=my-token
    ports:
      - 9090:9090
      - 8080:8080
    depends_on:
      - influx
    restart: always



  influx:
    image: influxdb:2
    networks:
      - default
    volumes:
      - influxdb-data:/var/lib/influxdb2
      - influxdb-config:/etc/influxdb2
    environment:
      - DOCKER_INFLUXDB_INIT_MODE=setup
      - DOCKER_INFLUXDB_INIT_USERNAME=my-username
      - DOCKER_INFLUXDB_INIT_PASSWORD=my-password
      - DOCKER_INFLUXDB_INIT_ORG=wot
      - DOCKER_INFLUXDB_INIT_BUCKET=my-bucket
      - DOCKER_INFLUXDB_INIT_ADMIN_TOKEN=my-token
    ports:
      - 8086:8086
    restart: always
```