# -*- coding: utf-8 -*-
import csv
from datetime import datetime, timedelta
from influxdb_client import InfluxDBClient, Point
from influxdb_client.client.write_api import SYNCHRONOUS
import sys

# InfluxDB credentials and connection details
org = "wot"  # Replace with your organization name
token = "my-token" #"vaimee-neph-u1j6ssp54oty9ay"  # Replace with your InfluxDB token
#url = "https://nephele.influx.vaimee.com:443"  # Replace with your InfluxDB URL
url = "http://localhost:8086"
csv_file="./neutrons2soilmoisture/model/bulkLoadData/bulkload.template.csv" #default, can be overriden
properties_dict = {
    'neutrons': 'neutrons',
    'muons': 'muons',
    'gamma': 'gamma',
    'integration_time(s)': 'integration_time',
    'V_in(Volt)': 'v_in',
    'temperature_in(°C)': 'temperature_in',
    'temperature_ext(°C)': 'temperature_ext',
    'ur(%)': 'ur',
    'pressure(hPa)': 'pressure'
}


#* PREFLIGHT
# CSV file path
if len(sys.argv)==2: #specifica path
    csv_file = sys.argv[1]  # 'data.csv'  # Replace with your CSV file path
    print("Loading csv: " + csv_file + " to " + url)
elif len(sys.argv)==3: #host
    if sys.argv[1]=="-h" or sys.argv[1]=="-H": #set host
        url= sys.argv[2]
    print("Loading default csv: " + csv_file + " to " + url)
elif len(sys.argv)>=4: #specifica path e host
    if sys.argv[1]=="-h" or sys.argv[1]=="-H": #set host
        url= sys.argv[2]
    csv_file = sys.argv[3]
    print("Loading csv: " + csv_file + " to " + url)
else: #Load default test csv
    print("Loading default csv: " + csv_file + " to " + url)


# Function to get the last date from the CSV file
def get_last_date(csv_file):
    last_date = None
    with open(csv_file, 'r', encoding='utf-8') as file:
        reader = csv.DictReader(file, delimiter=';')
        for row in reader:
            last_date = datetime.strptime(row['#Datetime'], '%Y-%m-%d %H:%M:%S')
    return last_date

# Get the last date in the CSV file
last_date = get_last_date(csv_file)
if last_date is None:
    print("No date found in CSV file.")
    sys.exit(1)

# Calculate the number of days to shift
today = datetime.today()
days_to_shift = (today - last_date).days

# Read the CSV file and create a new CSV with shifted dates
shifted_csv_file = csv_file.replace('.csv', '_shifted.csv')

with open(csv_file, 'r', encoding='utf-8') as infile, open(shifted_csv_file, 'w', encoding='utf-8', newline='') as outfile:
    reader = csv.DictReader(infile, delimiter=';')
    fieldnames = reader.fieldnames
    writer = csv.DictWriter(outfile, fieldnames=fieldnames, delimiter=';')
    writer.writeheader()

    for row in reader:
        row['#Datetime'] = (datetime.strptime(row['#Datetime'], '%Y-%m-%d %H:%M:%S') + timedelta(days=days_to_shift)).strftime('%Y-%m-%d %H:%M:%S')
        writer.writerow(row)

print(f"Created shifted CSV file: {shifted_csv_file}")
# Update the csv_file path to the new shifted CSV file
csv_file = shifted_csv_file






#* Re-read CSV and write data to respective buckets
num_records = 0
with open(csv_file, 'r', encoding='utf-8') as file:
    reader = csv.DictReader(file, delimiter=';')
    num_records = sum(1 for row in reader)

# Initialize InfluxDB client
client = InfluxDBClient(url=url, token=token)
write_api = client.write_api(write_options=SYNCHRONOUS)
batch_size = 2000  # Set the batch size as per your requirement
points_dict = {bucket: [] for bucket in properties_dict.values()}
last_timestamp = None

with open(csv_file, 'r', encoding='utf-8') as file:
    reader = csv.DictReader(file, delimiter=';')
    total = 0
    counter = 0
    delta = round(num_records / 10)
    print("Uploading " + str(num_records) + " records...")

    for row in reader:
        timestamp = datetime.strptime(row['#Datetime'], '%Y-%m-%d %H:%M:%S')

        if last_timestamp and timestamp == last_timestamp:
            print(f"Skipping record with duplicate timestamp: {timestamp}")
            continue

        # Create points for each column's data
        for header, value in row.items():
            if header == '#Datetime':
                continue

            mapped_header = properties_dict[header]
            point = Point(mapped_header.strip()).time(timestamp, 's')

            if header in ["neutrons", "muons", "gamma", "integration_time(s)"]:
                safe_value = value.rsplit('.', 1)[0]
                point.field("value", int(safe_value))  # Assuming all values are numeric, adjust as per data type
            else:
                point.field("value", float(value))

            points_dict[mapped_header].append(point)

        # Write in batches for each bucket
        for bucket, points in points_dict.items():
            if len(points) >= batch_size:
                try:
                    write_api.write(bucket, org, points)
                    print(f"Written {len(points)} points to bucket: {bucket}")
                    points.clear()
                except Exception as e:
                    print(f"Failed to write batch to bucket {bucket} ending with timestamp: {timestamp}")
                    print(e)

        last_timestamp = timestamp

        # Print counter for user
        if counter > delta:
            counter = 0
            print("Uploaded " + str(total) + "/" + str(num_records))
        else:
            counter += 1
        total += 1

    # Write any remaining points for each bucket
    for bucket, points in points_dict.items():
        if points:
            try:
                write_api.write(bucket, org, points)
            except Exception as e:
                print(f"Failed to write the final batch to bucket {bucket}")
                print(e)

# Close connection
client.close()
print("Uploaded " + str(num_records) + " records to InfluxDB!")
