#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
WoT application to expose a Thing that provides the data for the prediction
"""

from neutrons2soilmoisture.run import run

async def convert_handler(params):
    print("Converting neutrons to soil moisture")
    soil_moisture= await run(consumed_vos["crns"].servient.influxdb.execute_query)
    # the main reason to have a property that keep the last moisture
    # is to use in a trasparent way the influxDB history for that value
    await exposed_thing.properties['lastSoilMoistureConversion'].write(soil_moisture)     
    return soil_moisture
