from influxdb_client import InfluxDBClient
from influxdb_client.client.flux_table import FluxTable
import pandas as pd
from io import StringIO

"""url = "http://localhost:8086"  # Replace with your InfluxDB URL
token = "my-token"             # Replace with your InfluxDB token
org = "wot"                 # Replace with your InfluxDB organization

client = InfluxDBClient(url=url, token=token, org=org)
query_api = client.query_api()"""


date_col="#Datetime"
properties_dict={
    "neutrons":"neutrons",
    "muons":"muons",
    "gamma":"gamma",
    "integration_time":"integration_time(s)",
    "v_in":"V_in(Volt)",
    "temperature_in":"temperature_in(°C)",
    "temperature_ext":"temperature_ext(°C)",
    "ur":"ur(%)",
    "pressure":"pressure(hPa)"
}


def get_table_timestamps(queryFunction,property="neutrons"):
    tables = queryFunction(query='from(bucket:"'+property+'") |> range(start: -1y)')
    values = []
    for table in tables:
        for record in table.records:
            values.append(record.values["_time"])
    return values

def get_table_values(queryFunction,property):
    tables = queryFunction(query='from(bucket:"'+property+'") |> range(start: -1y)')
    values = []
    for table in tables:
        for record in table.records:
            values.append(record.values["_value"])
        #print(f"Last records: {table.records[-1].values} || {table.records[-2].values}", flush=True)
    return values

#One per hour
def resample_dataframe(df):
    df[date_col] = pd.to_datetime(df[date_col])
    df.set_index(date_col, inplace=True)
    df_resampled = df.resample('h').mean()
    df_resampled.reset_index(inplace=True)
    df_resampled[date_col] = df_resampled[date_col].dt.tz_localize(None) #remove +00:00
    return df_resampled

def get_history_as_csv_string(queryFunction,resample=True) -> str:
    print("Getting influx values")
    df= pd.DataFrame()
    timestamps= get_table_timestamps(queryFunction)
    df[date_col]=timestamps
    print(f"Length of timestamps: {len(timestamps)}")

    for propertyName in properties_dict:
        values= get_table_values(queryFunction,propertyName)

        #Fix different size arrays
        print(f"Length of values for {propertyName}: {len(values)}")
        if len(timestamps)==len(values):
            df[properties_dict[propertyName]]= values
        else:
            #if convert is called while influx is being updated, some values array could be longer than others
            print("Data arrays have different sizes,resizing...")
            #print(f"Last two elements of timestamps: {timestamps[-2:]}", flush=True)
            #print(f"Last two elements of values: {values[-2:]}", flush=True)

            if len(values)>len(timestamps):
                #cut last values
                print("Cutting last values...")
                values = values[:len(timestamps)]
            else:
                #copy last value and append
                print("Appending last values...")
                values.extend([values[-1]] * (len(timestamps) - len(values)))

            #Now add the resized arr
            df[properties_dict[propertyName]]= values




    print("Resampling influx values to 1 per hour")
    if resample: df=resample_dataframe(df) #reduce to one record per hour

    csv_buffer = StringIO()
    df.to_csv(csv_buffer, sep=";", index=False)

    # Get the CSV string
    csv_string = csv_buffer.getvalue()
    return csv_string


#CALL FUNCTION
#print(get_history_as_csv_string(query_api.query))