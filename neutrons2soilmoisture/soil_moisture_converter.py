from datetime import datetime, timedelta
import pandas as pd
import io


def calculate_sm_yesterday(csv_string) -> float:
    csv_data = io.StringIO(csv_string)
    csv_out= pd.read_csv(csv_data, delimiter=",")

    # Calculate the date for yesterday
    today = datetime.now()
    yesterday_date_obj = today - timedelta(days=1)
    yesterday = yesterday_date_obj.strftime("%Y-%m-%d")

    # Extract dates and SM columns from the DataFrame
    dates = csv_out["DateTime"]
    SM = csv_out["SM"]
    SM_yesterday = []

    # Iterate through the dates and collect SM values for yesterday
    for date, sm_value in zip(dates, SM):
        day = date.split("T")[0]
        if day == yesterday:
            SM_yesterday.append(float(sm_value))

    # Calculate the mean of SM values for yesterday
    sum_sm = sum(SM_yesterday)
    SM_mean = sum_sm / len(SM_yesterday) if SM_yesterday else 0.0

    #print(SM_yesterday)
    return SM_mean