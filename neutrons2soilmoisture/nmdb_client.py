import requests
import pandas as pd
from datetime import datetime

def get_raw_jung_data() -> str:
    station="JUNG"
    start_date = "2023-04-01"
    stop_date = datetime.now().isoformat().split('T')[0]
    # PARSE DATES
    start_date_arr = start_date.split("-")
    stop_date_arr = stop_date.split("-")
    
    T_ini_d = start_date_arr[2]
    T_ini_m = start_date_arr[1]
    T_ini_y = start_date_arr[0]
    T_end_d = stop_date_arr[2]
    T_end_m = stop_date_arr[1]
    T_end_y = stop_date_arr[0]
    
    # MAKE REQUEST
    url = (
        f"http://nest.nmdb.eu/draw_graph.php?formchk=1&stations[]={station}&tabchoice=revori&dtype=corr_for_efficiency"
        f"&tresolution=60&yunits=0&date_choice=bydate&start_day={T_ini_d}&start_month={T_ini_m}&start_year={T_ini_y}"
        f"&start_hour=0&start_min=0&end_day={T_end_d}&end_month={T_end_m}&end_year={T_end_y}"
        f"&end_hour=23&end_min=59&output=ascii"
    )
    
    response = requests.get(url)
    data = response.text
    
    # CHECK DATA INTEGRITY
    if "Sorry, no data available" in data:
        raise ValueError("Sorry, no data available")
    
    return data


# Usage example:
# import asyncio
# data_of_today = asyncio.run(get_jung_data_of_today())
# print(data_of_today)
