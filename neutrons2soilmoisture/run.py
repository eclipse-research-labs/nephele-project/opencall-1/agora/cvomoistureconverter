from . import nmdb_client
from . import soil_moisture_converter
import subprocess
from . import flux_client
import os

async def run(influxQueryFunction) -> float:
    print("** Starting model execution...")
    scriptPath= os.environ["SCRIPT_PATH"]
    fileIOPath= os.environ["FILEIO_PATH"]
    #Check if dataIO folder is present, else create it
    if not os.path.exists(fileIOPath):
        os.makedirs(fileIOPath)



    #1. FETCH DATA
    print("Fetching data")
    #get jung data and save to file
    rawJungData= nmdb_client.get_raw_jung_data()
    with open(fileIOPath+'/in1.csv', 'w') as file:
        file.write(rawJungData)
    print("- Jung data saved to: "+fileIOPath+'/in1.csv')
    #get crns influx history
    rawCrnsData= flux_client.get_history_as_csv_string(influxQueryFunction, resample=True)
    with open(fileIOPath+'/in2.csv', 'w') as file:
        file.write(rawCrnsData)
    print("- Influx data saved to: "+fileIOPath+'/in2.csv')



    #2. APPLY CORRECTION WITH JUNG DATA
    #Apply correction to crns sensor data with Jung station data
    script_cwd= scriptPath.rsplit('/', 1)[0]
    #logs = subprocess.run(['Rscript', scriptPath, fileIOPath], cwd=script_cwd,capture_output=True, text=True)
    #print(logs)
    #get output
    print("Running R model at path: "+scriptPath)
    try:
        logs = subprocess.run(
            ['Rscript', scriptPath, fileIOPath],
            cwd=script_cwd,
            capture_output=True,
            text=True,
            check=True  # This will raise a CalledProcessError if the command returns a non-zero exit code
        )
        #print(logs)
    except subprocess.CalledProcessError as e:
        # This block will execute if there is an error
        print("Error occurred while running the subprocess:")
        print("Return code:", e.returncode)
        print("Output:", e.output)
        print("Error output:", e.stderr)

    result=""
    try:
        with open(fileIOPath+'/out.csv', 'r') as file:
            result= file.read()
    except:
        raise Exception(f"Output file not found, R logs: {logs}")



    #3. CALCULATE SOIL MOISTURE
    print("Calculating soil moisture")
    soil_moisture= soil_moisture_converter.calculate_sm_yesterday(result)


    #Return results
    print("Execution finished! Returning soil moisture: "+str(soil_moisture))
    return soil_moisture