Scripts
====

This section describes the scripts contained in the `folder "scripts" <../scripts>`_

To run the scripts correctly, your position should be the root folder of this repository.
-------------

================  ==============================================
Script             Description
================  ==============================================
init.sh           In order to install all the necessary dependencies.
CVoMC.sh          In order to run the Vo.
dockerbuild.sh    Build the docker image from the dokerfile
dockerrun.sh      Run the docker image (remember to fill your .env before)
populateyaml.sh   Used by the doker image to set the influx credential and run the Vo.
runTestClient.sh  In order to run the test client.
================  ==============================================

