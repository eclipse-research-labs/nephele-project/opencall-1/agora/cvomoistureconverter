#!/usr/bin/env python
# -*- coding: utf-8 -*-

import asyncio
import logging

from wotpy.wot.servient import Servient
from wotpy.wot.wot import WoT

logging.basicConfig()
LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


async def main():
    wot = WoT(servient=Servient())
    consumed_thing = await wot.consume_from_url('http://localhost:9091/moisture-converter')

    statusmqtt = await consumed_thing.invoke_action('convert')
    LOGGER.info('Converted: {}'.format(statusmqtt))

    
    LOGGER.info('#### Test ends')

if __name__ == '__main__':
    loop = asyncio.run(main())
