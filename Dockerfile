FROM python:3.11-slim

WORKDIR /app

# Install R
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y r-base
RUN R -e "install.packages(c('pacman', 'dplyr', 'plotly', 'xts', 'ggplot2', 'scales', 'lubridate', 'hydroGOF', 'cowplot', 'signal'), repos='https://cloud.r-project.org/')"
RUN R -e "install.packages(c('readr'), repos='https://cloud.r-project.org/')"

# Install python libraries
RUN pip install vo-wot
RUN pip install python-dotenv

# Copy source
COPY mc.py /app/mc.py
COPY mc_td.json /app/mc_td.json
COPY cvo_mc_draft.yaml /app/cvo_mc_draft.yaml
#COPY scripts/populateyaml.sh /app/populateyaml.sh
# Copy r model scripts
COPY neutrons2soilmoisture /app/neutrons2soilmoisture
ENV PYTHONPATH=/app
#RUN chmod +x /app/populateyaml.sh
# Copy configurator
COPY populate_yaml.py /app/populate_yaml.py
COPY docker_cmd.sh /app/docker_cmd.sh
RUN chmod +x /app/docker_cmd.sh

CMD ["/app/docker_cmd.sh"]